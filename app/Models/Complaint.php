<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    use HasFactory;

    protected $fillable = [
        'victim_name',
        'incident_date',
        'location',
        'type_bullying',
        'report_content',
        'image',
        'addresed_to',
        'status',
        'reporter_id',
        'created_at',
        'updated_at',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class, 'reporter_id');
    }

    public function response()
    {
        return $this->hasMany(Response::class);
     }
}
