<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;
use App\Models\Complaint;
use App\Models\Response;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $user_6 = User::create([
            'name' => 'Maria Yuliani Setiawaty',
            'username' => 'maria',
            'password' => Hash::make('Mar_555'),
            'phone' => '0899887766',
            'level' => 'staff',
        ]);

        $user_7 = User::create([
            'name' => 'Marsella Natalia',
            'username' => 'marsella',
            'password' => Hash::make('Sek_777'),
            'phone' => '0998899789',
            'level' => 'admin',
        ]);

        $user_1 = User::create([
            'name' => 'Agustinus Marulitua Sijabat',
            'username' => 'agustinus',
            'password' => Hash::make('Agus_123'),
            'phone' => '08997753797',
            'level' => 'student',
        ]);

        $student_1 = Student::create([
            'nis' => '0088776545',
            'class' => '12',
            'gender' => 'male',
            'user_id' => $user_1->id
        ]);

        $complaint_1 = Complaint::create([
            'victim_name' => 'jimin',
            'incident_date' => '2022-12-10',
            'location' => 'Kamar mandi laki-laki di sekolah',
            'type_bullying' => 'verbal bullying',
            'report_content' => 'Saya lagi mau keluar kamar mandi tiba-tiba ada orang ngatain saya di deket wc laki-laki',
            'image' => 'bukti',
            'addresed_to' => 'Udin',
            'status' => 'not verified',
            'reporter_id' => $student_1->id,
        ]);

        $response_1 = Response::create([
            'complaint_id' => $complaint_1->id,
            'response_date' => '2022-12-12',
            'response' => 'Terima kasih atas pengaduan atas nama Agustinus kami akan menindak lanjuti',
            'staff_id' => $user_6->id,
        ]);

        $user_2 = User::create([
            'name' => 'Grace Angelina',
            'username' => 'grace',
            'password' => Hash::make('Grace_111'),
            'phone' => '0889977658',
            'level' => 'student',
        ]);

        $student_2 = Student::create([
            'nis' => '0088666545',
            'class' => '12',
            'gender' => 'female',
            'user_id' => $user_2->id
        ]);

        $complaint_2 = Complaint::create([
            'victim_name' => 'markonah',
            'incident_date' => '2021-11-09',
            'location' => 'Kamar mandi perempuan di sekolah',
            'type_bullying' => 'verbal bullying',
            'report_content' => 'Saya lagi mau keluar kamar mandi tiba-tiba ada orang ngatain saya di deket wc perempuan',
            'image' => 'bukti',
            'addresed_to' => 'Jeni',
            'status' => 'not verified',
            'reporter_id' => $student_2->id,
        ]);

        $response_2 = Response::create([
            'complaint_id' => $complaint_2->id,
            'response_date' => '2021-11-10',
            'response' => 'Terima kasih atas pengaduan atas nama Grace kami akan menindak lanjuti',
            'staff_id' => $user_6->id,
        ]);

        $user_3 = User::create([
            'name' => 'Christian',
            'username' => 'christian',
            'password' => Hash::make('Tian_222'),
            'phone' => '0889999888',
            'level' => 'student',
        ]);

        $student_3 = Student::create([
            'nis' => '0088556545',
            'class' => '12',
            'gender' => 'male',
            'user_id' => $user_3->id
        ]);

        $complaint_3 = Complaint::create([
            'victim_name' => 'yuta',
            'incident_date' => '2020-10-08',
            'location' => 'Kantin sekolah',
            'type_bullying' => 'physical bullying',
            'report_content' => 'Saya lagi mau jajan di kantin sekolah tiba-tiba saya dipukul sama asep',
            'image' => 'bukti',
            'addresed_to' => 'Asep',
            'status' => 'not verified',
            'reporter_id' => $student_3->id,
        ]);

        $response_3 = Response::create([
            'complaint_id' => $complaint_3->id,
            'response_date' => '2020-10-11',
            'response' => 'Terima kasih atas pengaduan atas nama Christian kami akan menindak lanjuti',
            'staff_id' => $user_6->id,
        ]);

        $user_4 = User::create([
            'name' => 'Amelia Venesa',
            'username' => 'amelia',
            'password' => Hash::make('Mel_333'),
            'phone' => '0998877876',
            'level' => 'student',
        ]);

        $student_4 = Student::create([
            'nis' => '0088446545',
            'class' => '12',
            'gender' => 'female',
            'user_id' => $user_4->id
        ]);

        $complaint_4 = Complaint::create([
            'victim_name' => 'suga',
            'incident_date' => '2019-09-07',
            'location' => 'Di sosial media',
            'type_bullying' => 'cyber bullying',
            'report_content' => 'Saya mendapatkan komentar buruk dari teman saya yang bernama Siti di sosial media',
            'image' => 'bukti',
            'addresed_to' => 'Siti',
            'status' => 'not verified',
            'reporter_id' => $student_4->id,
        ]);

        $response_4 = Response::create([
            'complaint_id' => $complaint_4->id,
            'response_date' => '2019-09-12',
            'response' => 'Terima kasih atas pengaduan atas nama Amelia kami akan menindak lanjuti',
            'staff_id' => $user_6->id,
        ]);

        $user_5 = User::create([
            'name' => 'Regina Arta Uli Simarmata',
            'username' => 'regina',
            'password' => Hash::make('Gin_444'),
            'phone' => '0778865769',
            'level' => 'student',
        ]);

        $student_5 = Student::create([
            'nis' => '0088336545',
            'class' => '12',
            'gender' => 'female',
            'user_id' => $user_5->id
        ]);

        $complaint_5 = Complaint::create([
            'victim_name' => 'saepuloh',
            'incident_date' => '2018-08-17',
            'location' => 'Di sosial media',
            'type_bullying' => 'cyber bullying',
            'report_content' => 'Saya mendapatkan komentar buruk dari teman saya yang bernama Dewi di sosial media',
            'image' => 'bukti',
            'addresed_to' => 'Dewi',
            'status' => 'not verified',
            'reporter_id' => $student_5->id,
        ]);

        $response_5 = Response::create([
            'complaint_id' => $complaint_5->id,
            'response_date' => '2018-08-15',
            'response' => 'Terima kasih atas pengaduan atas nama Regina kami akan menindak lanjuti',
            'staff_id' => $user_7->id,
        ]);
    }
}
