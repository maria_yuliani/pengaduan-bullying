<?php
if (auth()->user()) {
    $level = auth()->user()->level;
} else {
    $level = '';
}
?>

<header class="p-3 mb-2 border-bottom d-print-none" id="navigation-bar">
    <div class="container-fluid d-flex align-items-center">
        <a href="#" class="d-flex align-items-center m-2 text-decoration-none">
            <img src="/img/logo-abg.png" style="width: 50px">
        </a>
        <span class="fs-4 ms-5 me-5"></span>
        <ul class="nav me-auto" style="margin-left: 450px;">
                <li><a href="home" class="nav-link">Home</a></li>
                <li><a href="tentang" class="nav-link">Tentang</a></li>
                <li class="nav-item dropdown">
                <a class="me-3 nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                    aria-expanded="false">
                    Informasi
                </a>
                <ul class="dropdown-menu dropdown-menu-dark">
                    <li><a class="dropdown-item" href="pengertian">Pengertian</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="jenis">Jenis bullying</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="sanksi">Sanksi bullying</a></li>
                </ul>
                <li><a class="nav-link" href="/student/complaints/create">Lapor</a>
                <li><a href="/{{ $level }}/complaints" class="nav-link">Complaint</a></li>
                <li><a href="/{{ $level }}/responses" class="nav-link">Response</a></li>
                </li>
            @if (auth()->user() == null)
                <a href="/login"><button class="btn btn-success mt-1" style="margin-left: 50px">LOGIN</button></a>
            @endif
        </ul>
        @if (auth()->user())
            <a href="#" class="dropdown-toggle text-decoration-none" data-bs-toggle="dropdown">
                <i class="bi bi-person-circle" style="font-size: 35px"></i>
            </a>

            <ul class="dropdown-menu" style="font-size: 1.3rem;">
                <li><a href="#" class="dropdown-item">Halo, {{ auth()->user()->username }}</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                @if (auth()->user()->level == 'student')
                <li><a href="/student/complaints" class="dropdown-item">Laporan Saya</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li><a href="/student/responses" class="dropdown-item">Response</a></li>
                @endif
                @if (auth()->user()->level == 'admin')
                <li><a href="/admin/users" class="dropdown-item">User</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li><a href="/admin/students" class="dropdown-item">Student</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li><a href="/admin/complaint-report" class="dropdown-item">Generate</a></li>
                @endif
                @if (auth()->user()->level == 'staff')
                <li><a href="/staff/users" class="dropdown-item">User</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li><a href="/staff/students" class="dropdown-item">Student</a></li>
                @endif
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li><a href="/logout" class="dropdown-item"><i class="bi bi-box-arrow-left"></i> Log out</a></li>
        @endif
        </ul>
    </div>
</header>