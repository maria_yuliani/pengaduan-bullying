@extends('app')

@section('content')
    <div class="container mt-5">
        <h1 id="crud">Data Pengaduan</h1>
        <p>{{ $complaint_list->links() }}</p>
        <table class="table mt-5">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Victim_name</th>
                    <th>Incident_date</th>
                    <th>Location</th>
                    <th>Type_bullying</th>
                    <th>Image</th>
                    <th>Addresed_to</th>
                    <th>Status</th>
                    <th>Created_at</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($complaint_list as $complaint)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $complaint->victim_name }}</td>
                        <td>{{ $complaint->incident_date }}</td>
                        <td>{{ $complaint->location }}</td>
                        <td>{{ $complaint->type_bullying }}</td>
                        <td>{{ $complaint->image }}</td>
                        <td>{{ $complaint->addresed_to }}</td>
                        <td>{{ $complaint->status }}</td>
                        <td>{{ $complaint->created_at }}</td>
                        <td>
                            <a href="/staff/complaints/{{ $complaint->id }}" class="btn btn-info">Detail</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
