@extends('app')

@section('content')
    <div class="container mt-5 mb-5">
        <h1 id="crud">Detail response</h1>
        <form action="/staff/responses/{{ $response->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column mt-5">
                <div class="col-3 mb-3">
                    <label for="complaint_id" class="form-label">Complaint_id</label>
                    <input type="text" class="form-control" id="complaint_id" name="complaint_id" value="{{ $response->complaint_id }}" disabled>
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="response_date" class="form-label">Response_date</label>
                    <input type="text" class="form-control" id="response_date" name="response_date" value="{{ $response->response_date }}">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="response" class="form-label">Response</label>
                    <input type="text" class="form-control" id="response" name="response" value="{{ $response->response }}">
                </div>
            </div>
            <a href="/staff/responses/" class="btn btn-secondary">Back</a>
        </form>
    </div>
@endsection