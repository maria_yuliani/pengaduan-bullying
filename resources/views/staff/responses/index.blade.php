@extends('app')

@section('content')
    <div class="container mt-5">
        <h1 id="crud">Data Tanggapan</h1>
        <p>{{ $response_list->links() }}</p>
        <table class="table mt-5">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Complaint_id</th>
                    <th>Response_date</th>
                    <th>Response</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $response->complaint_id }}</td>
                        <td>{{ $response->response_date }}</td>
                        <td>{{ $response->response }}</td>
                        <td>
                            <a href="/staff/responses/{{ $response->id }}" class="btn btn-info">Detail</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/staff/responses/create" class="btn btn-success" id="add">Add New</a>
    </div>
@endsection
