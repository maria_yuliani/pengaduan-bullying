@extends('app')

@section('content')
    <div class="container mt-5 mb-5">
        <h1 id="crud">Detail user</h1>
        <form action="/staff/users/{{ $user->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column mt-5">
                <div class="col-3 mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" disabled>
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="{{ $user->username }}" disabled>
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="username" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password" value="{{ $user->password }}">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="username" class="form-label">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{ $user->phone }}">
                </div>
            </div>

            <div class="col-3 mb-3">
                <label class="form-label">Level</label>
                <select name="level" class="form-select">
                    @foreach (['admin', 'staff', 'student'] as $item)
                        <option value="{{ $item }}" {{ $user->level == $item ? 'selected' : '' }}>
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>

            <a href="/staff/users/" class="btn btn-secondary">Back</a>
        </form>
        @if ($errors->any())
        @foreach ($errors->all() as $error)
            <p class="text-danger">{{ $error}}</p>
        @endforeach
    @endif
    </div>
@endsection