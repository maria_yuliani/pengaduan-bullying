@extends('app')

@section('content')
    <header class="home-utama">
        <div class="container">
            <div class="row" id="judul-home">
                <div class="container-fluid">
                  <h1 id="welcome">HAI! WELCOME TO PROGRAM ABG!</h1>
                    </div>
                      <div class="col-6">
                        <img src="/img/nanya.png" style="width: 70%; margin-top: -200px;" id="mikir" alt="">
                      </div>
                      <div class="row">
                        <div class="col-6" style="text-align: left" id="apa-itu"> <br>
                            <p id="text-pengertian">Karena maraknya kasus bullying di lingkungan sekolah, dan sedang disemarakannya oleh pemerintah untuk mengurangi kasus bullying yang terjadi di sekolah. Jadi program ini untuk menindak lanjuti kasus bullying yang terjadi di sekolah Gracia. </p>
                            <br>
                            <h4>Jadi apa itu ABG?</h4> <br>
                            <h4>ABG adalah singkatan dari Anti Bullying Gracia, adalah layanan laporan kasus bullying yang terjadi di sekolah Gracia. pelapor dapat melaporankannya, dan dapat menerima respon dari petugas</h4>
                        </div>
                      </div>
                    </div><br><br><br><br><br><br><br><br><br><br><br><br>
                    <div class="row" style="margin-top: -50px;"> 
                      <div class="col-6" style="text-align: start">
                        <h3 id="sosmed">Program ini berguna untuk siswa dan siswi Gracia yang mungkin mengalami hal tidak menyenangkan yang dilakukan oleh teman sekelas, kakak kelas, ataupun adik kelas. Bullying bukan hanya dari dunia nyata, tapi bullying bisa terjadi di dunia maya lho! jangan salah banyak kasus bullying yang terjadi di dunia maya. Karena penyalahgunaan teknologi anak muda menggunakan dunia maya sebagai sarana mengujar kebencian.</h3>
                      </div>
                      <div class="col-6">
                        <img src="/img/sosmed.png" style="width: 70%; margin-top: -50px;" alt="">
                      </div>
                    </div>
                    <div class="container" style="margin-bottom: -200px;">
                      <div class="row" id="card-dampak" style="width: 1510px">
                        <div class="col-6">
                          <img src="/img/mulut.jfif" style="width: 70%; margin-left: -10px; margin-top: 95px;">
                        </div>
                        <div class="col-3">
                          <div class="card mx-3" style="width: 40rem; font-size: 20px;" id="card-verbal">
                            <div class="card-body">
                              <p id="card-judul">YUK LAPOR!</p>
                              <p class="card-text">Hey! yuk speak up, jangan takut dan jangan malu untuk menyampaikan laporanmu di program ini. kamu bisa sampaikan apa yang kamu alami lho!, kamu bisa tekan "Lapor" di atas untuk menyampaikan laporanmu . Apa sih yang harus dilakukan ketika ingin melapor?<br>1. Memiliki akun sebagai siswa dan bisa login. <br>2. Sampaikan laporanmu dengan menekan "Lapor" di atas <br>3. Isi setiap data dari form laporan dengan lengkap. <br>4. Sertakan bukti yang menguatkan bahwa kejadian itu benar adanya dan kenyataan. <br>5. Kirim laporanmu dengan menekan tombol "kirim"</p>
                            </div>
                        </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </header>
@endsection
