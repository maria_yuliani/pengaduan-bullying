@extends('app')

@section('content')
    <div class="container mt-5 mb-5">
        <h1 id="crud">Data Tanggapan</h1>
        <table class="table mt-5">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Complaint_id</th>
                    <th>Response_date</th>
                    <th>Response</th>
                    <th>Staff_id</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                @if($response->complaint->student->user->id == auth()->user()->id)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $response->complaint_id }}</td>
                        <td>{{ $response->response_date }}</td>
                        <td>{{ $response->response }}</td>
                        <td>{{ $response->staff_id }}</td>
                    </tr>
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
