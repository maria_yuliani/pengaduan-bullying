@extends('app')

@section('content')
    <header class="home-utama">
        <div class="container">
            <div class="row" id="judul-home">
                <div class="container col mt-5">
                    <h1 id="hubungi">Hubungi Kami</h1>
                    <div class="form-grup">
                        <div class="row mt-4">
                            <div class="col-8 mt-3" id="kontak-foto">
                                <img src="img/gbr-kontak-kami.png" id="foto-konmi" rows="12">
                            </div>
                            <div class="col-8 mt-3" id="isian">
                                <input id="komen" class="alert alert-secondary form-control subject" role="alert" type="text"
                                    style="border-radius: 0" placeholder="Komentar">
                                <label for="Nama"></label>
                                <input class="alert alert-secondary form-control nama" role="alert" type="text"
                                    style="border-radius: 0" placeholder="Nama">
                                <label for="Email"></label>
                                <input class="alert alert-secondary form-control email2" role="alert" type="text"
                                    style="border-radius: 0" placeholder="Email">
                                <button type="submit" class="btn btn-primary mt-4 kirim" style="border-radius: 0" role="alert"
                                    type="text" id="kirim">Kirim</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row kontak-logo align-items-strech mb-5 text-center">
                    <div class="col-2 mt-5">
                        <button type="button" href="" class="btn btn-primary email" style="border-radius: 100%;"><i class="bi bi-envelope-at-fill"></i></button>
                        <h6><b>Email</b></h6>
                        <p>smkgracia@gmail.com</p>
                    </div>
                    <div class="col-2 mt-5">
                        <button type="button" href="" class="btn btn-primary email" style="border-radius: 100%;"><i class="bi bi-telephone-fill"></i></button>
                        <h6><b>Phone</b></h6>
                        <p>+62 812 3456 7890</p>
                    </div>
                    <div class="col-2 mt-5">
                        <button type="button" href="" class="btn btn-primary email" style="border-radius: 100%;"><i class="bi bi-instagram"></i></i></button>
                        <h6><b>Instragram</b></h6>
                        <p>sekolah gracia</p>
                    </div>
                    <div class="col-2 mt-5">
                        <button type="button" href="" class="btn btn-primary email" style="border-radius: 100%;"><i class="bi bi-tiktok"></i></i></button>
                        <h6><b>Tiktok</b></h6>
                        <p>sekolah gracia</p>
                    </div>
                    <div class="col-2 mt-5">
                        <button type="button" href="" class="btn btn-primary email" style="border-radius: 100%;"><i class="bi bi-youtube"></i></i></button>
                        <h6><b>Youtube</b></h6>
                        <p>sekolah gracia</p>
                    </div>
                    <div class="col-2 mt-5">
                        <button type="button" href="" class="btn btn-primary email" style="border-radius: 100%;"><i class="bi bi-geo-alt-fill"></i></button>
                        <h6><b>Location</b></h6>
                        <p>Kota Bandung, Jawa Barat</p>
                    </div>
                </div>
            </div>
        </div>
        
            </div>
        </div>
    </header>
    </div>
@endsection
