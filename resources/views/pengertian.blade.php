@extends('app')

@section('content')
    <header class="home-utama">
        <div class="container">
            <div class="row" id="judul-home">
                <div class="container text-center mt-5">
                    <div class="row" id="pengertian-bully">
                      <div class="col-6">
                        <img src="/img/orangbingung.png" style="width: 30%" id="orangbingung" alt="">
                      </div>
                      <div class="col-6">
                        <img src="/img/bully-fisik.jfif" style="width: 30%" id="fisik" alt="">
                      </div>
                      <div class="col">
                        <img src="/img/bullying-satu.jfif" style="width: 31%" id="bullying" alt="">
                      </div>
                      <div class="row">
                        <div class="col-6" style="text-align: left" id="apa-itu">
                            <h1 id="text-apa">APA ITU BULLYING?</h1>
                            <p id="text-pengertian">Bullying adalah segala suatu bentuk penindasan, kekerasan dan perilaku yang membuat seseorang merasa tidak nyaman, merasa terancam dan juga merasa tertekan, yang dilakukan oleh seorang atau kelompok kepada seorang atau kelompok yang lebih lemah. </p>
                            <br>
                            <p id="text-lho">Banyak jenis kasus bullying yang terjadi di Indonesia terutama pada seorang pelajar, bentuk bullying bukan hanya memukul dan melukai fisik doang lho!</p>
                            <br>
                            <p id="yuk">Yuk! baca tentang jenis-jenis bullying</p>
                            <a href="jenis" class="http:/127.0.0.1:8000/jenis">
                              <button type="button" class="btn btn-primary" id="button">Selengkapnya</button>
                            </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="row ms-2" style="text-align: center; margin-bottom: -350px; margin-top: 200px;">
            <h1 id="dampak" style="text-align:left" class="ms-2">DAMPAK BAGI KORBAN BULLYING</h1>
          <div class="card mx-3" style="width: 17rem;" id="dampaknya">
              <img src="/img/depresi.jfif" class="card-img-top mt-3" alt="...">
              <div class="card-body">
                <p id="card-judul">GANGGUAN MENTAL</p>
                <p class="card-text">Korban bully sering kali menunjukkan adanya gejala masalah psikologis, bahkan setelah perundungan berlangsung. Kondisi yang paling sering muncul adalah depresi dan gangguan kecemasan. </p>
              </div>
          </div>
          <div class="card mx-2" style="width: 17rem;" id="dampaknya">
              <img src="/img/insom.jfif" class="card-img-top mt-3" alt="...">
              <div class="card-body">
                  <p id="card-judul">INSOMNIA</p>
                <p class="card-text">Para korban bullying sering kali kesulitan untuk tidur yang nyenyak. Sekalipun bisa tidur, tidak jarang waktu tersebut justru dihiasi dengan mimpi buruk. dan tak jarang mereka mempunyai insomnia.</p>
              </div>
          </div>
          <div class="card mx-3" style="width: 17rem;" id="dampaknya">
              <img src="/img/bully-fisik.jfif" class="card-img-top mt-3" alt="...">
              <div class="card-body">
                  <p id="card-judul">LUKA FISIK</p>
                <p class="card-text">Bukan hanya memar, korban bullying sering mengalami kecemasan yang dapat memicu stres pada tubuh. Kondisi ini bisa menyebabkan berbagai masalah kesehatan, seperti lebih sering sakit.</p>
              </div>
          </div>
          <div class="card mx-1" style="width: 17rem;" id="dampaknya">
              <img src="/img/sendiri.jfif" class="card-img-top mt-3" alt="...">
              <div class="card-body">
                <p id="card-judul">MENYENDIRI</p>
                <p class="card-text">Korban bullying, secara tidak langsung ditempatkan pada status sosial yang lebih rendah dari rekan-rekannya. Hal ini membuat korban bully menjadi sering merasa kesepian, dan berujung pada turunnya rasa percaya diri.</p>
              </div>
          </div>
          <div class="card mx-4" style="width: 17rem;" id="dampaknya">
              <img src="/img/bundir.png" style="height: 245px" class="card-img-top mt-3" alt="...">
              <div class="card-body">
                  <p id="card-judul">BUNUH DIRI</p>
                <p class="card-text">Dampak terparah adalah anak berusia sekolah yang meninggal dunia akibat bunuh diri setelah dirundung oleh teman-teman sepantarannya. Inilah bahaya bullying yang harus orangtua waspadai.</p>
              </div>
            </div>
          </div>
    </header>
@endsection
