@extends('app')

@section('content')
    <header class="home-utama">
        <div class="container">
            <div class="row" id="judul-homeblade">
                <div class="col" id="kata">
                    <h1 class="display-6" id="abg">ABG</h1>
                    <h3>Anti Bullying Gracia</h3>
                    <h2 style="text-align: left" id="layanan">Layanan Pengaduan Bullying <br> Sekolah Gracia</h2>
                    <p id="apa">Apa itu program ABG?</p>
                    <a href="tentang" class="http:/127.0.0.1:8000/tentang">
                    <button type="button" class="btn btn-primary" id="button">Tentang Kami<i class="bi bi-arrow-right-short"></i></button>
                  </a>
                  </div>
                <div class="col" style="width: 50%">
                    <div class="form-grup" id="poster">
                        <div class="row mt-4">
                            <div class="col mt-3">
                                <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
                                    <div class="carousel-inner">
                                      <div class="carousel-item active">
                                        <img src="/img/poster.png.jfif" class="d-block w-100" alt="...">
                                      </div>
                                      <div class="carousel-item">
                                        <img src="/img/buddy.jfif" class="d-block w-100" alt="...">
                                      </div>
                                      <div class="carousel-item">
                                        <img src="/img/poster-dua.jfif" class="d-block w-100" alt="...">
                                      </div>
                                      <div class="carousel-item">
                                        <img src="/img/equal.jfif" class="d-block w-100" alt="...">
                                      </div>
                                    </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid" style="text-align: left" id="bully-sekolah">
              <div class="row">
                <div class="col-6">
                 <img src="/img/dibully.webp" style="width: 100%" alt="...">
                </div>
                <div class="col-6" id="kata-home">
                  <h2 id="sebagian">Sebagian besar kasus bullying terjadi pada seorang pelajar di lingkungan sekolah.</h2>
                  <p id="penyebab">Penyebabnya beraneka ragam, biasanya pelaku merasa dirinya lebih berkuasa dan lebih kuat dari orang yang dibullynya. jadi pelaku merasa bahwa dia bisa bebas seenaknya untuk memperlakukan orang yang lebih lemah darinya.</p>
                  <p id="penyebab">Seorang pelaku bullying selalu ingin mengontrol, mendominasi, dan tidak menghargai orang lain. Penyebab orang menjadi pelaku bullying beraneka macam, salah satu penyebab mereka melakukan bullying sebagai bentuk balas dendam atas kehidupan keluarga yang tidak harmonis.</p>
                 </div>
              </div>
              <div class="container">
                <div class="row" id="card-dampak" style="text-align: center; margin-bottom: -250px;">
                    <a href="pengertian" class="http:/127.0.0.1:8000/pengertian">
                        <h1 id="dampak" style="text-align:left" class="ms-2">DAMPAK BAGI KORBAN BULLYING</h1>
                      </a>
                  <div class="card mx-3" style="width: 17rem;" id="card-dampaknya">
                      <img src="/img/depresi.jfif" class="card-img-top mt-3" alt="...">
                      <div class="card-body">
                        <p id="card-judul">GANGGUAN MENTAL</p>
                        <p class="card-text">Korban bully sering kali menunjukkan adanya gejala masalah psikologis, bahkan setelah perundungan berlangsung. Kondisi yang paling sering muncul adalah depresi dan gangguan kecemasan. </p>
                      </div>
                  </div>
                  <div class="card mx-2" style="width: 17rem;" id="card-dampaknya">
                      <img src="/img/insom.jfif" class="card-img-top mt-3" alt="...">
                      <div class="card-body">
                          <p id="card-judul">INSOMNIA</p>
                        <p class="card-text">Para korban bullying sering kali kesulitan untuk tidur yang nyenyak. Sekalipun bisa tidur, tidak jarang waktu tersebut justru dihiasi dengan mimpi buruk. dan tak jarang mereka mempunyai insomnia.</p>
                      </div>
                  </div>
                  <div class="card mx-3" style="width: 17rem;" id="card-dampaknya">
                      <img src="/img/bully-fisik.jfif" class="card-img-top mt-3" alt="...">
                      <div class="card-body">
                          <p id="card-judul">LUKA FISIK</p>
                        <p class="card-text">Bukan hanya memar, korban bullying sering mengalami kecemasan yang dapat memicu stres pada tubuh. Kondisi ini bisa menyebabkan berbagai masalah kesehatan, seperti lebih sering sakit.</p>
                      </div>
                  </div>
                  <div class="card mx-1" style="width: 17rem;" id="card-dampaknya">
                      <img src="/img/sendiri.jfif" class="card-img-top mt-3" alt="...">
                      <div class="card-body">
                        <p id="card-judul">MENYENDIRI</p>
                        <p class="card-text">Korban bullying, secara tidak langsung ditempatkan pada status sosial yang lebih rendah dari rekan-rekannya. Hal ini membuat korban bully menjadi sering merasa kesepian, dan berujung pada turunnya rasa percaya diri.</p>
                      </div>
                  </div>
                  <div class="card mx-4" style="width: 17rem;" id="card-dampaknya">
                      <img src="/img/bundir.png" style="height: 245px" class="card-img-top mt-3" alt="...">
                      <div class="card-body">
                          <p id="card-judul">BUNUH DIRI</p>
                        <p class="card-text">Dampak terparah adalah anak berusia sekolah yang meninggal dunia akibat bunuh diri setelah dirundung oleh teman-teman sepantarannya. Inilah bahaya bullying yang harus orangtua waspadai.</p>
                      </div>
                  </div>
            </div>
        </div>
        <div class="row mt-5">
         
        </div>
    </header>
@endsection