@extends('app')

@section('content')
    <div class="container mt-5 mb-5">
        <h1 id="crud">Detail student</h1>
        <form action="/admin/students/{{ $student->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column mt-5">
                <div class="col-3 mb-3">
                    <label for="nis" class="form-label">Nis</label>
                    <input type="text" class="form-control" id="nis" name="nis" value="{{ $student->nis }}" disabled>
                </div>
            </div>

            <div class="col-3 mb-3">
                <label class="form-label">Class</label>
                <select name="class" class="form-select">
                    @foreach (['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'] as $item)
                        <option value="{{ $item }}" {{ $student->class == $item ? 'selected' : '' }}>
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-3 mb-3">
                <label class="form-label">Gender</label>
                <select name="gender" class="form-select">
                    @foreach (['male', 'female'] as $item)
                        <option value="{{ $item }}" {{ $student->gender == $item ? 'selected' : '' }}>
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="user_id" class="form-label">User_id</label>
                    <input type="text" class="form-control" id="user_id" name="user_id" value="{{ $student->user_id }}" disabled>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error}}</p>
            @endforeach
        @endif
    </div>
@endsection