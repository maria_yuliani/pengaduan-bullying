@extends('app')

@section('content')
    <div class="container mt-5">
        <h1 id="crud">Laporan Response</h1>
        <form action="/admin/response-report" method="GET">
            <div class="row mt-5">
                <label for="start" class="col-1 col-form-label">Dari</label>
                <div class="col-2">
                    <input type="date" class="form-control" id="start" name="start" value="{{ $start }}">                    
                </div>
                <label for="end" class="col-1 col-form-label">Sampai</label>
                <div class="col-2">
                    <input type="date" class="form-control" id="end" name="end" value="{{ $end }}">
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-success">Cari</button>
                </div>
            </div>
        </form>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Complaint_id</th>
                    <th>Response_date</th>
                    <th>Report_content</th>
                    <th>Response</th>
                    <th>Staff_id</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $response->complaint_id }}</td>
                        <td>{{ $response->response_date }}</td>
                        <td>{{ $response->complaint->report_content }}</td>
                        <td>{{ $response->response }}</td>
                        <td>{{ $response->staff_id }}</td>
                        <td>
                        </td>
                    </tr>
                    <div class="modal fade" id="modal-{{ $response->id }}" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Konfirmasi</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div> 
                                <div class="modal-body">
                                    <p>Response dengan ID {{ $response->id }} akan dihapus.</p>
                                    <p>Lanjutkan?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/responses/{{ $response->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Batal</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <button type="button" class="btn btn-secondary" style="margin-top: -180px" onclick="window.print()">
            Print
        </button>
    </div>
@endsection
