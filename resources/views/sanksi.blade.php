@extends('app')

@section('content')
    <header class="home-utama">
        <div class="container">
            <div class="row" id="judul-home">
                <h1 id="sanksi-text">SANKSI PELAKU BULLYING</h1>
                <div class="col-6" style="text-align: left">
                    <h4 id="marak">Karena semakin maraknya kasus bullying yang terjadi, pemerintah membuat sanksi bagi para pelaki bullying, seperti berikut;</h4>
                    <br>
                    <p id="isi-sanksi">➤ Jika tindakan bullying penganiayaan ini ringan bisa dijerat pasal 351 KUHP, dengan ancaman maksimal 2 tahun 8 bulan pidana penjara.</p>
                    <p id="isi-sanksi">➤ jika berbentuk pengeroyokan dapat dikenai pasal 170 KUHP.</p>
                    <p id="isi-sanksi">➤ Tindakan perundungan dilakukan di temapt umum, mempermalukan harkat martabat sesesorang bisa juga dikenai pasal 310 dan 311 KUHP.</p>
                    <h4 id="marak-dua">Sanksi bagi pelaku bullying di sekolah ;</h4>
                    <br>
                    <p id="isi-sanksi">➤ Bagi pelaku bullying di sekolah Gracia akan mendapatkan sindang bullying.</p>
                    <p id="isi-sanksi">➤ Sidang bullying dilaksanakan oleh Guru sekolah dan bisa di bantu oleh anggota Osis,</p>
                </div>
                <div class="col-6" id="foto-sanksi" style="width: 40%">
                    <img src="/img/poster-bully.jpg" class="d-block w-100" alt="...">
                </div>
            </div>
        </div>
    </header>
@endsection