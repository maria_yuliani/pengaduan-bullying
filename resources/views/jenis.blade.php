@extends('app')

@section('content')
    <header class="home-utama">
        <div class="container" id="card">
            <div class="row" id="judul-home">
                <h1 id="judul-jenis" style="text-align:left" class="ms-2">JENIS-JENIS BULLYING</h1>
                <div class="card mx-3" style="width: 17rem;" id="card-verbal">
                    <img src="/img/verbal.jfif" class="card-img-top mt-3" alt="...">
                    <div class="card-body">
                      <p id="card-judul">BULLYING VERBAL</p>
                      <p class="card-text">Bullying verbal misalnya, mengolok-olok teman ketika nilainya tidak bagus, menyebut teman dengan julukan yang tidak baik dan sebagainya. </p>
                    </div>
                </div>
                <div class="card mx-2" style="width: 17rem;" id="card-social">
                    <img src="/img/social.jfif" class="card-img-top mt-3" alt="...">
                    <div class="card-body">
                        <p id="card-judul">BULLYING SOCIAL</p>
                      <p class="card-text">Bullying satu ini biasanya dilakukan di belakang korban yang diintimidasi. Tujuannya untuk melukai reputasi sosial sesorang atau membuat orang lain merasa dipermalukan atau dihina.</p>
                    </div>
                </div>
                <div class="card mx-3" style="width: 17rem;" id="card-fisik">
                    <img src="/img/bully-fisik.jfif" class="card-img-top mt-3" alt="...">
                    <div class="card-body">
                        <p id="card-judul">BULLYING PHYSICAL</p>
                      <p class="card-text">Bullying fisik, misalnya melempari teman dengan alat tulis, menghadang teman saat akan lewat, bahkan tindakan yang lebih parah adalah memukul, menonjok dan sejenisnya.</p>
                    </div>
                </div>
                <div class="card mx-1" style="width: 17rem;" id="card-cyber">
                    <img src="/img/cyber.jfif" class="card-img-top mt-3" alt="...">
                    <div class="card-body">
                      <p id="card-judul">BULLYING CYBER</p>
                      <p class="card-text">Cyberbullying menggunakan metode komunikasi online, seperti mengirim pesan teks atau media sosial untuk mengancam, mempermalukan, melecehkan, atau merendahkan seseorang.</p>
                    </div>
                </div>
                <div class="card mx-4" style="width: 17rem;" id="card-sex">
                    <img src="/img/sexual.jfif" class="card-img-top mt-3" alt="...">
                    <div class="card-body">
                        <p id="card-judul">BULLYING SEXUAL</p>
                      <p class="card-text">Sexual bullying, memberikan komentar jorok, berperilaku vulgar, menyentuh, dan melibatkan hal-hal porno. Dalam kasus-kasus tertentu, sexual bullying dapat berujung pada pelecehan seksual.</p>
                    </div>
                </div>
            </div>
        </div>
    </header>
@endsection