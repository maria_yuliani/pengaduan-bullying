<html>

<head>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>

<body>
    <br><br><br><br>
    <div id="loginnya">
        <div class="row">
            <div class="col-4  mx-auto mt-5 p-3 bg-light border rounded" id="kotaklog">
                <h1 class="text-center mb-4" id="kata-login">Login</h1>
                <form action="/login" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" class="form-control" id="username" name="username">
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <button type="submit" class="btn btn-primary mb-4 mt-3" style="width: 480px;">Login</button>
                </form>
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <p class="text-danger">{{ $error }}</p>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <script src="{{ mix('/js/app.js') }}"></script>
</body>

</html>
